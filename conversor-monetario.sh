#!/bin/bash

echo "Moeda origem " $1.
echo "Moeda destino " $2.
echo "Valor " $3.

json=$(curl -G "https://exchangeratesapi.io/api/latest?base=$1" 2>/dev/null | jq '.rates.'$2'');
#echo $json

echo $3 ' ' $1 '=> ' $2
python -c 'print('$3'*'$json')'
