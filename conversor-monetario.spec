Name:           conversor-monetario
Version:        1.0
Release:        1%{?dist}
Summary:        Conversor de moeda de Origem para destino

License:        GPLv3+
URL:            http://planbwebdesign.com.br/%{name}
Source0:        %{name}-%{version}.tar.gz
BuildArch: noarch

Requires: curl
Requires: jq
      
%description 
Conversor de moeda de Origem para destino feito pelo Bruno

%prep
%autosetup

%build

%install
mkdir -p %{buildroot}%{_bindir}
cp conversor-monetario.sh %{buildroot}%{_bindir}/conversor-monetario
chmod +x %{buildroot}%{_bindir}/conversor-monetario

%files
%{_bindir}/conversor-monetario

%doc README.md
%license LICENSE

%changelog
* Wed May 16 2018 Fernando Lisboa <fernando.lisboacfernandes@gmail.com> 1.0-1
- Versão inicial
